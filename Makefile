all:

.PHONY: help build synth sim pnr
help: swim
	swim/target/debug/swim help
build: swim
	swim/target/debug/swim build
synth: swim
	swim/target/debug/swim synth
sim: swim
	swim/target/debug/swim sim
pnr: swim
	swim/target/debug/swim pnr
upload: swim
	swim/target/debug/swim upload
clean: swim
	swim/target/debug/swim clean


.PHONY: swim
swim:
	cd swim; cargo build
